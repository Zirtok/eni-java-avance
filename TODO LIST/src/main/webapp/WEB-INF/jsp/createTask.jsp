<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Task creation</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
</head>
<body>
<h2 class="bleu">Create task</h2>
		<table class="table table-hover table-striped">
			<tr>
				<th>Task</th>
			</tr>
			<c:forEach items="${tasks}" var="t">
					<tr>
						<td>${t.description}</td>
					</tr>
			</c:forEach>
		</table>

		<div>
			<form:form action="addTask" method="POST"
				cssClass="form-horizontal" modelAttribute="task">
				<div class="form-group">
					<div class="col-xs-10">
						<form:input path="description" placeholder="Description"
							cssClass="form-control" />
							<form:errors path="description"></form:errors>
					</div>
					<div class="form-group">			
					
					<div class="col-xs-10">
						<form:input type="date" path="dateTask" placeholder="Date"
							cssClass="form-control" />
							<form:errors path="dateTask"></form:errors>
					</div>
					<div class="col-xs-10">
						<form:input path="stateTask" placeholder="State"
							cssClass="form-control" />
							<form:errors path="stateTask"></form:errors>
					</div>
					<div class="col-xs-10">
						<form:checkboxes items = "${categories}" path = "categories" />
					</div>
				</div>
					<div class="col-xs-2">
						<button type="submit" class="btn btn-primary pull-right">Create</button>			
					</div>
				</div>
				
			</form:form>
		</div>
</body>
</html>