package fr.eni.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.eni.bean.User;

public interface UserDAO extends JpaRepository<User, Integer>{
	
	List<User> findByLogin(String login);
	
}
