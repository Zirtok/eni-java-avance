package fr.eni.bean;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class Category {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	@NotBlank(message="Name can't be empty")
	private String name;
	
	@ManyToMany(mappedBy = "categories")
	private List<Task> tasks = new ArrayList<Task>();	

	public Category(String name, List<Task> tasks) {
		super();
		this.name = name;
		this.tasks = tasks;
	}

	public Category() {
		super();
	}

	public Category(int id, String name, List<Task> tasks) {
		super();
		this.id = id;
		this.name = name;
		this.tasks = tasks;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Task> getTasks() {
		return tasks;
	}

	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return this.getName();
	}
	
	
}
