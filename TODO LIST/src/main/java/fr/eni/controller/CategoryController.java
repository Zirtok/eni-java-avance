package fr.eni.controller;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import fr.eni.bean.Category;
import fr.eni.bean.User;
import fr.eni.service.GestionCategory;
import fr.eni.service.GestionUser;

@Controller
public class CategoryController {
	
	@Autowired
	GestionCategory gc;
	
	@Autowired
	GestionUser gu;
	
	
	@PostConstruct
	private void init(){
	}
	
	@RequestMapping(value="/createCategory", method=RequestMethod.GET)
	public ModelAndView createCategory(){	
		if(gu.getConnectedUser().getId() >= 0) {
			List<Category> categories = gc.list();
			Category categ = new Category();
			ModelAndView mav =  new ModelAndView("createCategory", "categories", categories);
			mav.getModelMap().addAttribute("category", categ);
			return mav;
		} else {
			User user = new User();
			return new ModelAndView("connection", "user", user);
		}
	}
	
	@RequestMapping(value="/addCategory", method=RequestMethod.POST)
	public ModelAndView addCategory(@Valid @ModelAttribute("category") Category category, BindingResult bindingResult){		
		if(!bindingResult.hasErrors()) {
			gc.addCategory(category);	
			return createCategory();
		}else {
			List<Category> categories = gc.list();
			ModelAndView mav =  new ModelAndView("createCategory", "categories", categories);
			mav.getModelMap().addAttribute("category", category);
			return mav;
		}		
	}
}
