package fr.eni.controller;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import fr.eni.bean.User;
import fr.eni.service.GestionUser;

@Controller
@SessionAttributes("connectedUser")
public class ConnectionController {
	@Autowired
	GestionUser gu;
	
	@PostConstruct
	private void init(){
	}

	@RequestMapping(path="/connection", method=RequestMethod.GET)
	public ModelAndView connection() {
		User user = new User();
		return new ModelAndView("connection", "user", user);
	}

	
	@RequestMapping(path="/connectionValid", method=RequestMethod.POST)
	public ModelAndView connectionValid(@Valid @ModelAttribute("user") User user, BindingResult bindingResult) {
		System.out.println(user.getLogin() + "   " + user.getPassword());
		List<User> list = gu.getUserByLogin(user);
		if(list.size() == 1 && list.get(0).getPassword().compareTo(user.getPassword()) == 0) {
			gu.setConnectedUser(list.get(0));
			System.out.println(gu.getConnectedUser().toString());
//			String url = "\"redirect:/app/afficher?index=" + gu.getConnectedUser().getId();
			return new ModelAndView("redirect:/app/afficher?index=" + gu.getConnectedUser().getId());
		}
		return new ModelAndView("connection", "user", user);
	}
	
	@ModelAttribute("connectedUser")
	public User getUserConnected() {
		return gu.getConnectedUser();
	}
	
	
}
