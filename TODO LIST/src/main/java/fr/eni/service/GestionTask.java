package fr.eni.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.eni.bean.Task;
import fr.eni.dao.TaskDAO;

@Service
public class GestionTask {
	@Autowired
	TaskDAO dao;
	
	public Task gettask(int id) {
		return dao.findOne(id);
	}
	
	public List<Task> listtask() {
		return dao.findAll();
	}
	
	public void addtask(Task t) {
		dao.save(t);
	}
	
	public void removeTask(Task t) {
		dao.delete(t);;
	}
	
	public void update(Task t) {
		dao.save(t);
	}
	
	public List<Task> findByDate(String date) {
		List<Task> list = null;
		
	    Date dateParse;
		try {
			dateParse = new SimpleDateFormat("MM/dd/yyyy").parse(date);
			list = dao.findByDateTaskContaining(dateParse);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
		
		
		
		return list;
	}
	
	public List<Task> findByCategory(int indexCat) {
		List<Task> list = null;
		
		list = dao.findByCategoriesContaining(indexCat);
		
		return list;
	}
	
	public List<Task> orderBy(String by) {
		List<Task> list = null;
		switch (by) {
		case "da": list = dao.findByOrderByDateTaskAsc(); break;
		case "dd": list = dao.findByOrderByDateTaskDesc(); break;
		case "ca": list = dao.findByOrderByCategoriesAsc(); break;
		case "cd": list = dao.findByOrderByCategoriesDesc(); break;
		
		default: list = dao.findAll();
		}
		return list;
	}

}
