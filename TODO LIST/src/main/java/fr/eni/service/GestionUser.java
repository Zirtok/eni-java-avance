package fr.eni.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.eni.bean.User;
import fr.eni.dao.UserDAO;

@Service
public class GestionUser {
	@Autowired
	UserDAO dao;
	User connectedUser = new User(-1,"0","0","0","0",null,null);
	
	public User getUser(int id) {
		return dao.findOne(id);
	}
	
	public List<User> listUser() {
		return dao.findAll();
	}
	
	public void addUser(User u) {
		dao.save(u);
	}
	
	public List<User> getUserByLogin(User u) {
		return dao.findByLogin(u.getLogin());
	}
	
	public User getConnectedUser() {
		return this.connectedUser;
	}
	
	public void setConnectedUser(User u) {
		this.connectedUser = u;
	}
	
	public void disconnectUser() {
		this.connectedUser = new User();
	}
	
}
