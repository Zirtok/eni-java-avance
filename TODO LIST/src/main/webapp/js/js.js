$(function(){	
	$( function() {
	    $( ".datepicker" ).datepicker({
	    	dateFormat: "dd/MM/yyyy",
		    onSelect: function(dateText, inst) {
		        var date = $(".datepicker").val();
		        window.location="filterByDate?date=" + date;
		    }
		});
	  });
	
	$(".filterByCategory").change(function(){
		var att = $(".filterByCategory option:selected").val();
		console.log(att);
		window.location="filterByCategory?indexCat=" + att;
	});	
});