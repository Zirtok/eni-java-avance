<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>task manager</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="../css/commun.css">
	
</head>
<body>
<div class="container">	
	<h1 class="bleu">Connection</h1>
	<br><br>
		<div>
<form:form action="connectionValid" method="POST" cssClass="form-horizontal" modelAttribute="user">
				<div class="form-group">			
					<form:label path="login">Login :</form:label>
					<div>
						<form:input path="login"  placeholder="login"/> 
					</div>
					<div>
						<form:errors path="login" cssClass="erreur"/> 
					</div>
				</div>
				<div class="form-group">			
					<form:label path="password">Password :</form:label>
					<div>
						<form:input path="password"  placeholder="password"/> 
					</div>
					<div>
						<form:errors path="password" cssClass="erreur"/> 
					</div>
				</div>
				
				<div class="form-group" >
					<div>
						<button type="submit" class="btn btn-primary">Connection</button>			
					</div>
				</div>	
				
				${connectedUser}

				
			
	</form:form>
	</div>
</div>

</body>
</html>