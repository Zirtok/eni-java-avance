package fr.eni.controller;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import fr.eni.bean.Category;
import fr.eni.bean.Task;
import fr.eni.bean.User;
import fr.eni.service.GestionCategory;
import fr.eni.service.GestionTask;
import fr.eni.service.GestionUser;

@Controller
public class TaskController {
	
	@Autowired
	GestionTask gt;
	
	@Autowired
	GestionCategory gc;
	
	@Autowired
	GestionUser gu;
	
	@PostConstruct
	private void init(){
		
	}
	
	@RequestMapping(value="/orderBy", method=RequestMethod.GET)
	public ModelAndView orderTasks(String by){	
		List<Task> tasks = gt.orderBy(by);
		return new ModelAndView("tasksTable", "tasks", tasks);
	}
	
	@RequestMapping(value="/filterByCategory", method=RequestMethod.GET)
	public ModelAndView filterByCategory(int indexCat){		
		List<Task> tasks = gt.findByCategory(indexCat);
		ModelAndView mav = new ModelAndView("tasksTable", "tasks", tasks);
		
		List<Category> categories = gc.list();
		mav.getModelMap().addAttribute("categories", categories);
		
		return mav;
	}
	
	@RequestMapping(value="/filterByDate", method=RequestMethod.GET)
	public ModelAndView filterByDate(String date){		
		List<Task> tasks = gt.findByDate(date);
		ModelAndView mav = new ModelAndView("tasksTable", "tasks", tasks);
		
		List<Category> categories = gc.list();
		mav.getModelMap().addAttribute("categories", categories);
		
		return mav;
	}
	
	@RequestMapping(value="/createTask", method=RequestMethod.GET)
	public ModelAndView createTask(){	
		System.out.println(gu.getConnectedUser().getId());
		if(gu.getConnectedUser().getId() >= 0) {
			Task task = new Task();
			ModelAndView mav = new ModelAndView("createTask", "task", task);
			List<Category> categories = gc.list();
			mav.getModelMap().addAttribute("categories", categories);
			List<Task> tasks = gt.listtask();
			mav.getModelMap().addAttribute("tasks", tasks);
			return mav;		
		} else {
					User user = new User();
					return new ModelAndView("connection", "user", user);
		}
	}	
	
	@RequestMapping(value="/addTask", method=RequestMethod.POST)
	public ModelAndView addTask(@Valid @ModelAttribute("task") Task task, BindingResult bindingResult){		
		if(!bindingResult.hasErrors()) {
			gt.addtask(task);		
			return createTask();
		}else {
			ModelAndView mav = new ModelAndView("createTask", "task", task);
			List<Category> categories = gc.list();
			mav.getModelMap().addAttribute("categories", categories);
			List<Task> tasks = gt.listtask();
			mav.getModelMap().addAttribute("tasks", tasks);
			return mav;		
		}				
	}
	
	@RequestMapping(value="/deleteTask", method=RequestMethod.GET)
	public ModelAndView deleteTask(int id){		
		Task task = gt.gettask(id);
		gt.removeTask(task);
		return new ModelAndView("redirect:/app/afficher");
	}
	
	
	
	@ModelAttribute("connectedUser")
	public User getUserConnected() {
		System.out.println(gu.getConnectedUser().toString());
		return gu.getConnectedUser();
	}
}
