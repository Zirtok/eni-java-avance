package fr.eni.dao;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.eni.bean.Task;

public interface TaskDAO extends JpaRepository<Task, Integer>{
	
	List<Task> findByOrderByDateTaskDesc();
	List<Task> findByOrderByDateTaskAsc();
	List<Task> findByOrderByCategoriesDesc();
	List<Task> findByOrderByCategoriesAsc();
	List<Task> findByDateTaskContaining(Date dateTask);
	List<Task> findByCategoriesContaining(int indexCat);
}
