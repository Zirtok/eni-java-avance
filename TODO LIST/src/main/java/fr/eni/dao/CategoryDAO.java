package fr.eni.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.eni.bean.Category;

public interface CategoryDAO extends JpaRepository<Category, Integer>{

}
