package fr.eni.controller;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import fr.eni.bean.Category;
import fr.eni.bean.User;
import fr.eni.service.GestionCategory;
import fr.eni.service.GestionUser;

@Controller
@SessionAttributes("user")
public class UserController {
	@Autowired
	GestionUser gu;
	
	@Autowired
	GestionCategory gc;
	
	@PostConstruct
	private void init(){
		
	}
	
	@RequestMapping(value="/afficher", method=RequestMethod.GET)
	public ModelAndView afficher(Integer index) {
		User user = null;
		if (index != null){
			user = gu.getUser(index);			
			ModelAndView mav = new ModelAndView("tasksTable", "tasks", user.getTasks());
			List<Category> categories = gc.list();
			mav.getModelMap().addAttribute("categories", categories);
			return mav;		
		}
		else
			return new ModelAndView("redirect:/app/connection");
	}
	
	
	@ModelAttribute("connectedUser")
	public User getUserConnected() {
		System.out.println(gu.getConnectedUser().toString());
		return gu.getConnectedUser();
	}
}
