package fr.eni.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.eni.bean.Category;
import fr.eni.dao.CategoryDAO;

@Service
public class GestionCategory {
	
	@Autowired
	CategoryDAO dao;
	
	public void addCategory(Category c) {
		dao.save(c);
	}
	
	public void deleteCategory(Category c) {
		dao.delete(c);
	}
	
	public void updateCategory(Category c) {
		dao.save(c);
	}
	
	public List<Category> list(){
		return dao.findAll();
	}
	
	public List<Category> listByUser(){
		return dao.findAll();
	}

}
