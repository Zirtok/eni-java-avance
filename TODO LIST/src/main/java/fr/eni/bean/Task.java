package fr.eni.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Task {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	@NotBlank(message="Description can't be empty")
	private String description;
	@DateTimeFormat(pattern = "yyyy-MM-dd")	
	@Temporal(TemporalType.DATE)
	private Date dateTask;
	@NotBlank(message="State can't be empty")
	private String stateTask;
	
	@ManyToMany (cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch=FetchType.EAGER)
	@JoinTable(name = "task_categories",
    joinColumns = @JoinColumn(name = "task_id"),
    inverseJoinColumns = @JoinColumn(name = "category_id")
	)
	private List<Category> categories  = new ArrayList<Category>();
	
	public Task(String description, Date dateTask, String stateTask, List<Category> categories) {
		super();
		this.description = description;
		this.dateTask = dateTask;
		this.stateTask = stateTask;
		this.categories = categories;
	}

	public Task() {
		super();
	}

	public Task(int id, String description, Date dateTask, String stateTask, List<Category> categories) {
		super();
		this.id = id;
		this.description = description;
		this.dateTask = dateTask;
		this.stateTask = stateTask;
		this.categories = categories;
	}

	public String getdescription() {
		return description;
	}

	public void setdescription(String description) {
		this.description = description;
	}

	public Date getdateTask() {
		return dateTask;
	}

	public void setdateTask(Date dateTask) {
		this.dateTask = dateTask;
	}

	public String getStateTask() {
		return stateTask;
	}

	public void setStateTask(String stateTask) {
		this.stateTask = stateTask;
	}

	public List<Category> getCategories() {
		return categories;
	}

	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}

	public int getId() {
		return id;
	}
}
