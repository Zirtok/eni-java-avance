<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Liste des tâches</title>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="../js/js.js"></script>
</head>
<body>
${connectedUser}
<a href="createTask">Create task</a>
<a href="createCategory">Create category</a>
<label>Select category</label>
<p>Date: <input type="text" class="datepicker filterByDate"></p>
<select class="filterByCategory">
    <c:forEach items="${categories}" var="c">
    	<option value="${c.id}">${c.name}</option>
    </c:forEach>
</select>
<c:choose>
	<c:when test="${not empty tasks}">
		<table class="table table-hover table-striped">
		<tr>					
			<th>Title</th>
			<th>Date</th>
			<th>State</th>
			<th>Categories</th>
			<th>Delete</th>
		</tr>		
			<c:forEach items="${tasks}" var="t">
				
				<tr>
					<td>${t.description}</td>
					<td>${t.dateTask}</td>
					<td>${t.stateTask}</td>
					<td>
						<c:forEach items="${t.categories}" var="tc">
							${tc.name}
						</c:forEach>
					</td>
					<td><a href="deleteTask?id=${t.id}">Delete</a></td>
				</tr>
			</c:forEach>
		</table>
	</c:when>		
	<c:otherwise>
		<p>No task.</p>
	</c:otherwise>
	</c:choose>
	

</body>
</html>