<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Category creation</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<h2 class="bleu">Create category</h2>
		<table class="table table-hover table-striped">
			<tr>
				<th>Category</th>
			</tr>
			<c:forEach items="${categories}" var="c">
					<tr>
						<td>${c.name}</td>
					</tr>
			</c:forEach>
		</table>

		<div>
			<form:form action="addCategory" method="POST"
				cssClass="form-horizontal" modelAttribute="category">
				<div class="form-group">
					<div class="col-xs-10">
						<form:input path="name" placeholder="Name"
							cssClass="form-control" />
						<form:errors path="name"></form:errors>
					</div>
					<div class="col-xs-2">
						<button type="submit" class="btn btn-primary pull-right">Create</button>			
					</div>
				</div>
				
			</form:form>
		</div>
</body>
</html>